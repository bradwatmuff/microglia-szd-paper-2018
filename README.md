# Microglia SZD paper 2019

Code written for ImageJ and R to analyze imaging data from phalloidin microglia/neuron co-culture experiments in Sellgren et al 2019.

Code makes use of existing functions in both the ImageJ (Fiji) and R projects, and is therefore licensed similarly.

Pipeline for usage:

1. ImageJ mask macro
2. ImageJ analyze particles macro
3. ImageJ normalization macro
4. R particle analysis script

Code contains absolute paths which refer to BWs host machine and which would need to be edited prior to external usage. These might be generalized in a later update.

The R script contains references to 'plate 5', one of the plates analyzed. These references were changed as required for the analysis of other plate data.

Raw experimental data in .csv format available upon request.