# graphing actin objects in each FOV of Plate 5

# loading data frame from raw ImageJ analysis
library(readr)
particle_counts <- read_csv("~/Desktop/CoCult Images Plates 5-7/Plate 5 Results/particle_counts.csv")

# getting number of rows (ie number of objects) for each FOV
particle_counts_lean <- data.frame(table(particle_counts$Label))
View(particle_counts_lean)

#extract well label and add to data frame
well <- substr(particle_counts_lean$Var1, 24, 26)
particle_counts_lean$well <- well

#loading normalization data from raw imageJ files
well_intensity <- read_csv("~/Desktop/CoCult Images Plates 5-7/Plate 5 Results/well_intensity.csv")

# extract relevant measurements and add in well data
well_intensity_lean <- well_intensity[, c(1,2,23)]
well2 <- substr(well_intensity_lean$Label, 24, 26)
well_intensity_lean$well <- well2
well_intensity_lean2 <- well_intensity_lean[, c(2:4)]
order_well <- order(well_intensity_lean2$Label)
sorted_well_intensity_lean2 <- well_intensity_lean2[order_well, ]
View(sorted_well_intensity_lean2)

#Remove last row of each data frame
particle_counts_lean <- particle_counts_lean[-c(217), ]
sorted_well_intensity_lean2 <- sorted_well_intensity_lean2[-c(217), ]

# normalize data by averaging well instensity and comparing to each well's particle counts
normailized_counts <- particle_counts_lean$Freq/(sorted_well_intensity_lean2$IntDen/1000000)
particle_counts_lean_normalized <- particle_counts_lean
particle_counts_lean_normalized$Freq <- normailized_counts
View(particle_counts_lean_normalized)

# plotting number of objects vs each FOV
library(ggplot2)
p1 <- ggplot(particle_counts_lean, aes(well, Freq)) +
  geom_boxplot(outlier.shape = NA) +
  scale_y_continuous(limits = c(0, 1000)) +
  geom_jitter(position=position_jitter(width=.1, height=0))
p1

# and normalized plot:
p2 <- ggplot(particle_counts_lean_normalized, aes(well, Freq)) +
  geom_boxplot(outlier.shape = NA) +
  scale_y_continuous(limits = c(0, 1000)) +
  geom_jitter(position=position_jitter(width=.1, height=0))
p2

#Further pruning particle_counts_lean_normalized
#Getting rid of infinite data points
particle_counts_lean_normalized2 <- particle_counts_lean_normalized[is.finite(particle_counts_lean_normalized$Freq), ]

#Getting rid of ridiculous values > 60000 Objects / fov
particle_counts_lean_normalized3 <- particle_counts_lean_normalized2[particle_counts_lean_normalized2$Var1!="Jess20x Plate 5 260718_C02_s28.tif", ]

#Graphing means
library(ggpubr)
p3 <- ggline(particle_counts_lean_normalized3, x = "well", y = "Freq", 
      add = c("mean_se", "jitter"), 
      ylab = "Freq", xlab = "Well")
p3

#calculating ANOVA/multiple comparison and statistical significance
res.aov <- aov(Freq ~ well, data = particle_counts_lean_normalized3)
summary.aov(res.aov)
p4 <- TukeyHSD(res.aov)
p4

# Arranging plots in a grid

# Defining multiplot for ggplot2
multiplot <- function(..., plotlist=NULL, file, cols=1, layout=NULL) {
  library(grid)
  
  # Make a list from the ... arguments and plotlist
  plots <- c(list(...), plotlist)
  
  numPlots = length(plots)
  
  # If layout is NULL, then use 'cols' to determine layout
  if (is.null(layout)) {
    # Make the panel
    # ncol: Number of columns of plots
    # nrow: Number of rows needed, calculated from # of cols
    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
                     ncol = cols, nrow = ceiling(numPlots/cols))
  }
  
  if (numPlots==1) {
    print(plots[[1]])
    
  } else {
    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))
    
    # Make each plot, in the correct location
    for (i in 1:numPlots) {
      # Get the i,j matrix positions of the regions that contain this subplot
      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
      
      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
    }
  }
}

multiplot(p1, p3, p2, cols = 2)
